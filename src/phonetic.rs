//! Data structures used for modeling phonetic environments
pub mod environment;
pub mod matrix;
