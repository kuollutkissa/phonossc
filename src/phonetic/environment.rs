use std::ops::Index;

use super::matrix::Matrix;

pub struct WordEnvironment {
    pub segments: Vec<Matrix>,
    pub syllables: Vec<usize>,
    pub syllable_matrices: Vec<Matrix>,
}

impl WordEnvironment {
    pub fn new(
        segments: Vec<Matrix>,
        syllables: Vec<usize>,
        syllable_matrices: Vec<Matrix>,
    ) -> WordEnvironment {
        Self {
            segments,
            syllables,
            syllable_matrices,
        }
    }
    pub fn replace(&mut self, at: usize, len: usize, with: &[Matrix]) {
        self.segments.delete_chain(at, len);
    }
}
/// Indexes the segments of the word.
/// Panics if `index` is greater than the segment count of the word
impl Index<usize> for WordEnvironment {
    type Output = Matrix;
    fn index(&self, index: usize) -> &Self::Output {
        &self.segments[index]
    }
}

impl<T: Clone> ChainOps<T> for Vec<T> {
    fn insert_chain(&mut self, chain: &[T], index: usize) {
        let mut current_index = index;
        while current_index < index + chain.len() {
            self.insert(current_index, chain[current_index]);
            current_index += 1
        }
    }
    fn delete_chain(&mut self, index: usize, len: usize) {
        let mut len = len;
        while len > 0 {
            _ = self.remove(index);
            len -= 1
        }
    }
}
/// Used for modifying environments;
pub trait ChainOps<T: Clone> {
    fn insert_chain(&mut self, chain: &[T], index: usize);
    fn delete_chain(&mut self, index: usize, len: usize);
}
