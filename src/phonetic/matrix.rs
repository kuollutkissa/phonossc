//! Data structures and helper functions related to matrices
use std::{
    collections::HashMap,
    ops::{Deref, DerefMut},
};
/// Represents a collection of features of a single segment
#[derive(Clone)]
pub struct Matrix(
    /// Feature map
    pub HashMap<String, Option<usize>>,
);

impl Matrix {
    /// Constructs a new matrix
    pub fn from(features: HashMap<String, Option<usize>>) -> Matrix {
        Self(features)
    }
    /// Sets the value of all features in `self` which are Some() in `other` to that value.
    pub fn overriden_with(self, other: &Matrix) -> Matrix {
        let mut res = self.clone();
        for (key, val) in &**other {
            _ = res.insert(key.to_string(), val.clone());
        }
        res
    }
}

impl Deref for Matrix {
    type Target = HashMap<String, Option<usize>>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Matrix {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
